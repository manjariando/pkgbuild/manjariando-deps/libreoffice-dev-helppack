# Maintainer: tioguda <guda.flavio@gmail.com>

_pkgbase=helppack
pkgbase=libreoffice-dev-${_pkgbase}
_pkgnamefmt=LibreOffice
_LOver=7.2.3.2
pkgver=$(cut -f1-4 -d'.' <<< ${_LOver})
pkgrel=1
arch=('x86_64')
license=('LGPL3')
url="https://www.libreoffice.org"
pkgdesc="Help pack for LibreOffice development branch"
makedepends=('imagemagick')

_languages=(
  'ar     "Arabic"'
  'bg     "Bulgarian"'
  'bs     "Bosnian"'
  'cs     "Czech"'
  'da     "Danish"'
  'de     "German"'
  'el     "Greek"'
  'en-GB  "English (British)"'
  'en-ZA  "English (South African)"'
  'es     "Spanish"'
  'et     "Estonian"'
  'fi     "Finnish"'
  'fr     "French"'
  'he     "Hebrew"'
  'hr     "Croatian"'
  'hu     "Hungarian"'
  'id     "Indonesian"'
  'is     "Icelandic"'
  'it     "Italian"'
  'ja     "Japanese"'
  'km     "Khmer"'
  'ko     "Korean"'
  'nl     "Dutch"'
  'pl     "Polish"'
  'pt-BR  "Portuguese (Brazilian)"'
  'pt     "Portuguese (Portugal)"'
  'ro     "Romanian"'
  'ru     "Russian"'
  'sk     "Slovak"'
  'sl     "Slovenian"'
  'tr     "Turkish"'
  'uk     "Ukrainian"'
  'vi     "Vietnamese"'
)

_libreoffice_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=libreoffice$(cut -f1-2 -d'.' <<< ${_LOver}) %U
Categories=Office;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_libreoffice_desktop" | tee org.libreoffice.desktop
}

pkgname=()
source=()
_url=https://dev-builds.libreoffice.org/pre-releases/rpm

for _lang in "${_languages[@]}"; do
  _locale=${_lang%% *}
  _pkgname=libreoffice-dev-help-${_locale,,}
  
  pkgname+=($_pkgname)
  source_x86_64+=("$_url/x86_64/${_pkgnamefmt}_${_LOver}_Linux_x86-64_rpm_${_pkgbase}_${_locale}.tar.gz"
                    "https://metainfo.manjariando.com.br/libreoffice-dev-${_pkgbase}/org.libreoffice-help-${_locale}.metainfo.xml"
                    "https://metainfo.manjariando.com.br/libreoffice-dev-${_pkgbase}/help.png")

sha256sums_x86_64=('465823d9483bfb8b5465a0395eea98e23868dcc1a07dd91266ce904740f32fdd'
                   '61c05072f724a0b03a4e982770b451899bd8bf24d039f693ab4f6331161d6d98'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   'fbf305a710a3d147c536dcd175c4c53a1a1001de28c68baf8e4477ba0a0693f8'
                   '30f58581c6efd462f0381aac155239c1b61133c2fff18f881df600b41d76ea0f'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '0e7c6338998d583473d334b562575cc402c6c0c67696b012685b657a6ed1206d'
                   '3248b96ac21f7f567f170b8fe39a7d721e415e8772db99a7fd8396471184ecf8'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   'ca15ac86acb3ef1f44cb026daa677c07af013f0100b6655340278dfe2c418fed'
                   'a331b0426a28e7887d0224cca1ff82aa67190567d51e657aa26c003eeb562df1'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '5f57319f384405664c60910666ef8abcd37c12d35902aa6a2ce1e4f06994656e'
                   'b46cd0d961a6c45108bdd173cd08da105d9f7b7bb2bcd96f2c3e425801b66b67'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '6dac17efc606d18fefa8ad1df7972334a93030f1f787a4a009cf422e00160ade'
                   'bd367cca8ff16fdc1a65559f3319bc7a758ecf42a8ee2561c12fab5a23b54a50'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '0b65852f49d6b2da47ff35ea784e82eb09a28a3ec6ddaa4e3620815a84751708'
                   'eb1d7cd4d04b3b30c372ecf171410ec872239ab8dfc2962ddb94d2570fac77ae'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   'ac22f1e9c5f5709d7cf5d7c62f2f405216051c9c3097efab273c0feff9b3e115'
                   '16f069eef2bb7722ee1241cee1c2cf3a4132c21b675bcadce9fba26424139b83'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   'd209814843888c70d3e678565856b6f1c99c6eeb69863d3e9baa2f5a63786b9e'
                   'b36086ed3ed1110e908c671f9442323c09164bae7e07b9404b5c018c8bcdca7c'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '95876c8063e229846664bc755f09b303460dcbc9c97c93ab2e041cdc2c4a0080'
                   '0b1355c0e08890e2a634bfd6aca9477be352dd4cb99aff05cb53998c966a5113'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '629c5859d0cffce91ef929a24dbc99ac6c7e892f944ef010b75c4a2483f2f96b'
                   'c49b817a706c025a043e4a36cc5dc5e34d0811729920f1210199987cba3795b7'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   'a18eba88e039088d368a577b7b723005aef68a862664a8b6f91f59a7730b4aff'
                   'f007b025acece4350de7b68f5763c6a53e51a089e6a4c8cab5cd55064f3859ec'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '39765fade01b7b0d5a3adb11a69df8250c022db34316820ec5199fb73135760b'
                   'ef20b201a0d3aef0a582d6b8c98840c3f229e1ec18f50c1f9d601a5124efdf72'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '059860e399644abb62055e868d29e91d7b67d3771a14d314bddee27192307dce'
                   '3797464fe95c3bd0fc95088883d693a1d7fbfe9877e406016a589043993c0407'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '783c154f323548700625563ae0fe7780ebe000e8391a71eb8fcbff1a47b508c9'
                   'aafb093a9ae2de57d7a5a30ab1f438e139c1cc0a496b6b693c62e3a9e3f83e70'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '59ce4032b6e2fccfacee4b610d34ce7f7af59e3ead3f1550e350d1a269b66122'
                   '7681e8a8fbcc8535568eab309cf94e10b85a94a8dbc086177eaeba827ae76446'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '18e1526cc4eb3563004043bd51197a88062304ebdf765fd4928b9786fa9b544e'
                   '9d50242cf427059a77f03f6ce18b3773d6db6c47e494913a7c7586928d5a7269'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '3f4c6305c15c1dc5a74c00bd11c6cb226153fd0267ae947d3c678ba9eab92945'
                   '184f45657e59bf3978acff07a94f5cf6061d37d037eb95b62cd57e73dbe76812'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   'b65fffa3e7001d81040825c9ed72b770115433985ee1dafe007929b0679a215e'
                   '16b6784155ab40201997c41a1e0f2b15d0409d115914559673ee31711c7acff2'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '18bd0f1e6e8f0c2b3c5a8cec9ba0f36cbe0efef74bc1c3152ba6e3376d7ac56c'
                   '2b4d00a442874cb78165a225c29919b9c20b5e1313c3637ca2fc38663ee285b8'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '7603129dbfba90f9237fdfa66ba74a2f649543783abd10e2918ec179bab08fc8'
                   '4993f6b7c1e0f51c4fac590e23c7466c6e904a92bacade625697df004f578d3c'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '59ac0d7bca0caeca3c06a8b769dd01766c7abd626d38672b344b565f18c17e04'
                   '264efa92ca23af3421509a927033c4c4cb10ccad450123c37eef00707b3495b1'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '30b8e6006af6d2b4df1118b82cc8d17414112af977a5ce792b989a7ad65bbbdc'
                   '267dced45b8b5495b08db1e9d924309719a89339d350cb47fb7400d1cc0f5789'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   'bf3e1994e2cf3c61ec5eef6ce551becec1c5b01c2a1328188cf3bd69b8703dca'
                   'bdf54048455f544ac85f6fb0b9735b16ec04ce76c320445c757c895dda8ad6d1'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '6a93e01c254a9604fba088bf834f82f842c60b759fa5e112d633fb55c89b69e1'
                   '494c42febec3bf5f800e8a66e3557f30128dcfbbf25cb452011f1c55735d1cee'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   'ae5c935537f5a7d3936d7d0a095c11ad42a00aa8cbadf41d6dace85d7e8fec52'
                   'd6373441144e84c196d722bbb8eb874e35435d136d94d33b4d3b28c1b68d8fb0'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '7e643741ddaf213a860296888dc492aab5d62ebaacb0c13a9a7b6c42727fd29c'
                   '6cabaf184610330e3b64abe6ee079489eaae7f2ea89786651d08a9c648e2a782'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '2bd73fb90fab46dd1750421965bb80a9272a589d293b09f27614e754dc0c1547'
                   '326882f95203252b3653e7ae88394a734e753cf50b48a7b6aa080e06741d9f81'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   'da625f5aaff5e686876272dca91be2aae21ec43969795446bb1d3038d2b0a095'
                   '3d6dcbf97519c05409b4f45d72850637bb831c2e73632cd373a26ed986f28f16'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '50be83f23dad3e99305a41ad41f20aca4a9d5127752ad5e590abcabd162d2bc0'
                   '3b2042e21f5ec659fcec2403df2d4bee9ac09dc236140159549883e791888302'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   'b26666b8b8576be29ea85cd22cf034a4392fd19ff2ad8c2bb991b85694c268da'
                   '1febe9186f66ee261aa1db26e8cac721148fb26ffde9f84a04fdb670a9436ea8'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '1c4b968115c31d2b72cb3116b55aaf9eeddf757cb80c0e223776e8dc71f092e1'
                   '2a1f02526c14345e41fa92b81d83c97379fb14c72c582f07d63a3eefa56db5cc'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c'
                   '8c2a1f0577079b42ee41976c305fb56f794ee76fd6ee2d09621b6e6f23cc4c6c'
                   'd92ebfc77dab35825842f7ad5e90c13d6ae3ab44a4d236cfae58c9475d1f8356'
                   '70a91915ccdc9dd376c9c096682baab8afbb6f4553f4e862d2c83db83af8ab4c')
  eval "package_$_pkgname() {
    _package $_lang
  }"
done

_package() {
    pkgdesc="$2 help pack for LibreOffice development branch"
    __locale=$1
    depends=("libreoffice-dev>=${pkgver}")
    
    find "${srcdir}/${_pkgnamefmt}_${_LOver}"*${__locale}/RPMS/*rpm -exec bsdtar -x -f '{}' -C "${pkgdir}" \;

    # Appstream
    install -Dm644 "${srcdir}/org.libreoffice-help-$1.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.libreoffice-help-$1.metainfo.xml"
    install -Dm644 "${srcdir}/org.libreoffice.desktop" "${pkgdir}/usr/share/applications/org.libreoffice_help_${__locale/-/_}.desktop"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/help.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/libreoffice-dev-help-${1,,}.png"
    done
}
